#!/bin/python
"""Mimics an audio input device that can have text to speech pushed through it"""
import os
import sys
import multiprocessing as mp

import keyboard as kb
import pyttsx3 as tts
import dearpygui.dearpygui as gui

VIEWPORT = {
    'title': 'quick tts',
    'width': 600,
    'height': 300,
    'x': 0,
    'y': 0,
    'color': (100, 100, 100, 50)
}

WINDOW = {
    'name': 'tts'
}


def main(args=None):
    """The 'entry point' of the program, for better encapsulation"""
    speaker = tts.init()
    gui.create_context()

    with gui.window(tag=WINDOW['name'], no_background=True, no_title_bar=True,
                    no_resize=True, no_move=True):
        placeholder_text = ''
        gui.add_input_text(default_value=placeholder_text)

    gui.create_viewport(title=VIEWPORT['title'], width=VIEWPORT['width'],
                        height=VIEWPORT['height'], x_pos=VIEWPORT['x'],
                        y_pos=VIEWPORT['y'], resizable=False, decorated=False,
                        always_on_top=True, clear_color=VIEWPORT['color'])
    gui.setup_dearpygui()
    gui.show_viewport()
    # gui.set_primary_window(WINDOW['name'], True)
    gui.start_dearpygui()
    gui.destroy_context()


if __name__ == '__main__':
    main(sys.argv)
