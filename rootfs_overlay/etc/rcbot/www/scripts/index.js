// @ts-check

// Browser supports the Gamepad API
if(!navigator.getGamepads) {
    alert("You're browser may not support the gamepad api")
}

// Table global state
const table_elements = {}
/**
 * Update the diagnostics table with the given key/value pair
 *
 * @param {string} key
 * @param {any} value
 */
function updateLogTable(key, value) {
    const dp = new DOMParser()
    const html = `<div class="item" id="${key}">
    	<h4 class="key">${key}</h4>
        <p class="value">${value}</p>
    </div>`
    const content = dp.parseFromString(html, 'text/html')
    const table = document.querySelector("#diagnostics #data")

	// If the item doesn't exist, create it
    if(!table_elements[key]) {
        table_elements[key] = content.getElementById(key)
        table.append(table_elements[key])
    }

    table_elements[key].querySelector('.value').textContent = value
}

const gamepads = []
window.addEventListener("gamepadconnected", (event) => {
    console.log('Connected gamepad:')
    console.log(event.gamepad)

    gamepads.push(event.gamepad)
    // @ts-ignore
    event.gamepad.vibrationActuator.playEffect(
        "dual-rumble",
        {
            startDelay: 0,
            duration: 1000,
            weakMagnitude: 1.0,
            strongMagnitude: 1.0
        }
    )

    updateLogTable('Number of controllers', gamepads.length)
})

window.addEventListener("gamepaddisconnected", (event) => {
    gamepads.forEach((gamepad, index) => {
        if(gamepad.connected) {
            delete gamepads[index]
        }
    })

    updateLogTable('Number of controllers', gamepads.length);

    console.log('Disconnected gamepad:')
    console.log(event.gamepad)
})

var sent_msgs = 0
fetch('data/gamepad.json').then(resp => resp.json()).then(data => {
    var last_msg = JSON.stringify(data)
    console.log('fetch')
    window.setInterval(() => {
        if (gamepads.length <= 0) {
            return
        }
        let cntl = navigator.getGamepads()[gamepads[0].index]

        // update message
        data['Joystick X'] = cntl.axes[0]
        data['Joystick Y'] = cntl.axes[1]
        data['Button A'] = cntl.buttons[1].pressed
        data['Button B'] = cntl.buttons[0].pressed

        updateLogTable('Joystick Left X', data['Joystick X'])
        updateLogTable('Joystick Left Y', data['Joystick Y'])
        updateLogTable('A Button', data['Button A'])
        updateLogTable('B Button', data['Button B'])

        // Check message to reduce redundant request
        const data_str = JSON.stringify(data)
        if (last_msg === data_str) return
        fetch('gamepad', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: data_str
        }).then(resp => {console.log("Sent message!")})
        .catch(err => console.warn(`Fetch error: ${err}`))

        // Update last message
        last_msg = data_str
    }, 50)
})


