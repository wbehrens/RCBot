# RCBot

My [CAPS](https://yourcapsnetwork.org/) 2022 passion project
Objective: Design an rc-car and program the control system

## This Repo

What you'll find in this repo are the FreeCAD and stl models used in the design processes, as well as
the source code for the control system(both client and server). The [client](resources/) is a simple browser interface
in Javascript/HTML/CSS that makes use of the browser gamepad API

### Parts used

TODO

### Media

TODO

### Resources

* [Nerves docs](https://hexdocs.pm/nerves/getting-started.html)
* [Nerves website](https://nerves-project.org/)
