defmodule Rcbot.MixProject do
  use Mix.Project

  @app :rcbot
  @version "0.1.0"
  @all_targets [:rpi0]

  def project do
    [
      app: @app,
      version: @version,
      elixir: "~> 1.9",
      deps: deps(),
      build_embedded: true,
      start_permanent: Mix.env() == :prod,
      releases: [{@app, release()}],
      archives: [nerves_bootstrap: "~> 1.10"],
      preferred_cli_target: [run: :host, test: :host]
    ]
  end

  def application do
    [
      mod: {Rcbot, []},
      # started before main app
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp deps do
    [
      # Dependencies for all targets
      {:nerves, "~> 1.7.15", runtime: false},
      {:shoehorn, "~> 0.8.0"},
      {:ring_logger, "~> 0.8.3"},
      {:toolshed, "~> 0.2.13"},
      {:plug_cowboy, "~> 2.0"},
      {:jason, "~> 1.3"},

      # Dependencies for all targets except :host
      {:nerves_runtime, "~> 0.11.6", targets: @all_targets},
      {:vintage_net_wifi, "~> 0.10.0", targets: @all_targets},
      {:nerves_pack, "~> 0.7.0", targets: @all_targets},
      {:circuits_gpio, "~> 1.0", targets: @all_targets},
      {:circuits_i2c, "~> 0.1"},
      {:servo_kit, "~> 0.2.0"},

      # Dependencies for specific targets
      {:nerves_system_rpi0, "~> 1.18", runtime: false, targets: :rpi0}
    ]
  end

  def release do
    [
      overwrite: true,
      # Erlang distribution is not started automatically.
      # See https://hexdocs.pm/nerves_pack/readme.html#erlang-distribution
      cookie: "#{@app}_cookie",
      include_erts: &Nerves.Release.erts/0,
      steps: [&Nerves.Release.init/1, :assemble],
      strip_beams: Mix.env() == :prod or [keep: ["Docs"]]
    ]
  end
end
