defmodule Rcbot.Webserver do
  use Plug.Router
  require Jason
  require Integer
  require ServoKit

  # plug Plug.SSL # SSL is required by the gamepad api
  plug(Plug.Logger, log: :debug)
  plug(Plug.Static, from: "/etc/rcbot/www", at: "/")
  plug(:match)
  plug(:dispatch)

  get "/" do
    send_file(conn, 200, "/etc/rcbot/templates/index.html")
  end

  post "/gamepad" do
    {:ok, data, _conn} = read_body(conn)
    gamepad = Jason.decode!(data)
    x = gamepad["Joystick X"] # x for turning
    #a = gamepad["Button A"] # a for drive
    #b = gamepad["Button B"] # be for headlights

    # calcuate
    if Integer.is_even(x),
      do: ServoKit.set_pwm_duty_cycle(10, ch: 0),
      else: ServoKit.set_pwm_duty_cycle(2, ch: 0)
    #if a,
    #  do: Rcbot.Motor.run(),
    #  else: Rcbot.Motor.stop()
  end

  match _ do
    send_resp(conn, 404, "oops")
  end
end
