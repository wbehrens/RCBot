defmodule Rcbot.Motor do
  use GenServer
  require Logger
  require ServoKit

  @name Motor
  @pwm 0
  @in_1 1
  @in_2 2

  # Client
  def start_link do
    {:ok, ref} = ServoKit.start_link([address: 0x40, name: DriveMotor])
    GenServer.start_link(__MODULE__, ref, name: @name)
  end

  def run(invert \\ false) do
    GenServer.cast(@name, {:run, invert})
  end

  def stop do
    GenServer.cast(@name, :stop)
  end

  # Server (callbacks)
  def init(ref) do
    Logger.info("Starting #{@name} process")
    {:ok, ref}
  end

  def handle_cast({:run, _invert}, ref) do
    ServoKit.set_pwm_duty_cycle(ref, 100, ch: @pwm)
    ServoKit.set_pwm_duty_cycle(ref, 100, ch: @in_1)
    ServoKit.set_pwm_duty_cycle(ref, 0, ch: @in_2)
    {:noreply, ref}
  end

  def handle_cast(:stop, ref) do
    ServoKit.set_pwm_duty_cycle(ref, 0, ch: @pwm)
    {:noreply, ref}
  end
end
