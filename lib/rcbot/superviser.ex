defmodule Rcbot.Supervisor do
  use Supervisor
  require ServoKit
  require Plug.Cowboy
  require Rcbot.Motor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    children = [
      {Plug.Cowboy, scheme: :http, plug: Rcbot.Webserver, options: [ip: {0,0,0,0}, port: 8080]},
      {ServoKit, [address: 0x70]}, # servo
      #{Rcbot.Motor, []}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
