defmodule Rcbot do
  use Application
  require Logger

  def start(_type, _args) do
    Rcbot.Supervisor.start_link()
  end
end
